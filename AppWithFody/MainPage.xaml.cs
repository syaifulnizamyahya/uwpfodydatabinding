﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;

namespace AppWithFody
{
	public sealed partial class MainPage : Page
	{
		PersonViewModel viewModel = new PersonViewModel(); //required by x:Bind

		public MainPage()
		{
			InitializeComponent();
			//var viewModel = new PersonViewModel();
			DataContext = viewModel; // required by legacy binding
			var propertyChanged = (INotifyPropertyChanged) viewModel;
			propertyChanged.PropertyChanged += OnPropertyChanged;
		}
		
		void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			eventLog.Text += string.Format("PropertyName:{1}{0}", Environment.NewLine, e.PropertyName);
		}

		private void BindingUpdate_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
		{
			Bindings.Update(); //required by x:Bind, compiled binding
		}
	}
}
